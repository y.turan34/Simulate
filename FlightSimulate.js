var myApp  =  angular.module('myApp', []);
myApp.controller('jsonCtrl', function($scope,JsonService, $http){
    
    $scope.RealCards;
    $scope.Filter = "";
    $scope.OneworldChecked = true;
    $scope.StarAllianceChecked = true;
    $scope.SkyTeamChecked = true;
    
    function FlightCard(Company,FromCity,FromAirport,ToCity,ToAirport,Departs,Arrives,Operation,FlightNo,Class,Price,TotalDuration,ReturnBoolean){
        this.Company            = Company;
        this.FromCity           = FromCity;
        this.FromAirPort        = FromAirport;
        this.ToCity             = ToCity;
        this.ToAirport          = ToAirport;
        this.Departs            = Departs;
        this.Arrives            = Arrives;
        this.Operation          = Operation;
        this.FlightNo           = FlightNo;
        this.Class              = Class;
        this.Price              = Price;
        this.TotalDuration      = TotalDuration;
        this.ReturnBoolean      = ReturnBoolean;
    }
    
    getDataFunctionCb(this.$scope,JsonService,function(data){
        var cards = [];
        for(var i=0;i<data.cards.length;i++){
                for(var y=0;y<data.cards[i].departure.length;y++){
                    cards.push(new FlightCard(   data.cards[i].departure[y].alliance,
                                                 data.cards[i].departure[y].from,
                                                 data.cards[i].departure[y].from,
                                                 data.cards[i].departure[y].to,
                                                 data.cards[i].departure[y].to,
                                                 data.cards[i].departure[y].departureDateTime,
                                                 data.cards[i].departure[y].arrivalDateTime,
                                                 data.cards[i].departure[y].operatingAirlineCode,
                                                 data.cards[i].departure[y].flightNumber,
                                                 data.cards[i].departure[y].cabinClassInfo[0].class,
                                                 data.cards[i].price.itinerary.totalFare,
                                                 data.cards[i].departureFlightDuration,
                                                 false,));
                }
                for(var y=0;y<data.cards[i].return.length;y++){
                    cards.push(new FlightCard(   data.cards[i].return[y].alliance,
                                                 data.cards[i].return[y].from,
                                                 data.cards[i].return[y].from,
                                                 data.cards[i].return[y].to,
                                                 data.cards[i].return[y].to,
                                                 data.cards[i].return[y].departureDateTime,
                                                 data.cards[i].return[y].arrivalDateTime,
                                                 data.cards[i].return[y].operatingAirlineCode,
                                                 data.cards[i].return[y].flightNumber,
                                                 data.cards[i].return[y].cabinClassInfo[0].class,
                                                 data.cards[i].price.itinerary.totalFare,
                                                 data.cards[i].returnFlightDuration,
                                                 true,));
                }
                
            }
        cards=deleteDublicate(cards);
        $scope.cards = cards;
        $scope.RealCards = cards;
        
    });
        
    //Filters
    $scope.FilterForChipClicked = function() {
        $scope.Filter = "-Price"       
    };
    $scope.FilterForExpensiveClicked = function() {
        $scope.Filter = "+Price"       
    };
    $scope.FilterForMinDurationClicked = function() {
        $scope.Filter = "+TotalDuration"       
    };
    $scope.FilterForMaxDurationClicked = function() {
        $scope.Filter = "-TotalDuration"       
    };
    //Sort
$scope.Search = function() {    
    $scope.cards = $scope.RealCards; // cards'll always update !
    var tempCards = [];        
    if($scope.departureMin !== undefined && $scope.departureMax !== undefined){
        for(var i=0;i<$scope.cards.length;i++){
            if($scope.cards[i].ReturnBoolean == false){
                if(getHourValue($scope.cards[i].Departs,$scope.departureMin,$scope.departureMax)==true){
                   tempCards.push ($scope.cards[i]);
                }
            }
        }
       
    }
    if($scope.returnMin !== undefined && $scope.returnMax !== undefined){
        for(var i=0;i<$scope.cards.length;i++){
            if($scope.cards[i].ReturnBoolean == true){
                if(getHourValue($scope.cards[i].Departs,$scope.returnMin,$scope.returnMax)==true){
                    tempCards.push ($scope.cards[i]);
                }
            }
        }
    }
    if(tempCards.length == 0){
        tempCards = $scope.cards;
    }
    var tempCardsFilter = [];
    
    if($scope.OneworldChecked == true)
    {
        for(var i=0;i<tempCards.length;i++){
            if(tempCards[i].Company == "Oneworld"){
                tempCardsFilter.push(tempCards[i]);
            }
        }        
    }
    
    if($scope.StarAllianceChecked == true)
    {
        for(var i=0;i<tempCards.length;i++){
            if(tempCards[i].Company == "Star Alliance"){
                tempCardsFilter.push(tempCards[i]);
            }
        }        
    }
    
    if($scope.SkyTeamChecked == true)
    {
        for(var i=0;i<tempCards.length;i++){
            if(tempCards[i].Company == "SkyTeam"){
                tempCardsFilter.push(tempCards[i]);
            }
        }        
    }
    
    $scope.cards = tempCardsFilter;
    };
});

function getHourValue(DateValue,minExpect,maxExpect){
    thisDay = new Date(DateValue);
    thisDay = thisDay.getHours();
    if( minExpect < thisDay && maxExpect > thisDay){
        
        return true;
    }
}

function deleteDublicate(array){
        var tempArray=[]; 
        
        tempArray.push(array[0]);
        for(var i=1;i<array.length;i++){
            checker=false;
            for(var y=0;y<tempArray.length;y++){
                if(array[i].FlightNo == tempArray[y].FlightNo){
                    checker = true;
                }
            }
            if(checker == false){
                tempArray.push(array[i]);
            }
        }
        
        return tempArray;
    }

myApp.factory('JsonService',function($http){
        
    var getDataCb = function(callback){        
        $http({$method:"GET",url:"flightData.json"}).then(function(result){
            callback(result.data.data.epower);
        });
    }
    return {getDataCb:getDataCb};
});

var getDataFunctionCb = function($scope,JsonService,callback){

    var myDataPromise = JsonService.getDataCb(function(data){
        callback(data);        
    });
}